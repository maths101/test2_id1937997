package space;


/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet>{
	/**
	 * Implement the compareTo method from the Comparable<E> class
	 * @param antother Planet to compare
	 * @return -1 if the "this" comes first, 1 if it comes after, 0 is they are equal
	 * @author Grigor 1937979
	 *
	 */
	public int compareTo(Planet other) {
		if(this.equals(other) ){
			return 0;
		}
		if( this.name.compareTo(other.name) != 0 ) {
			return  this.name.compareTo(other.name);
		}
		if(this.radius > this.radius) {
			return -1;
		}
		return 1;
	}
	/**
	 * Override the equals method
	 * @param antother Planet to compare
	 * @return false if they are different, true if equal
	 * @author Grigor 1937979
	 *
	 */
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Planet)) {
			return false;
		} 
		Planet other = (Planet)obj;
		if(this.name.equals(other.name) && this.planetarySystemName.equals(other.planetarySystemName) ) {
			return true;
		}
		return false;
	}
	/**
	 * Override the hashCode method to make the hash consistent with equals method
	 * @return the hash representation of the object
	 * @author Grigor 1937979
	 *
	 */
	@Override
	public int hashCode() {
		String s= this.name + this.planetarySystemName;
		return s.hashCode();
	}
	
	
	
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	
}

