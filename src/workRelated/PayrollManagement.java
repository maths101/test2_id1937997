package workRelated;
/**
 * Application class to calculate the total expenses from employees
 * @author Grigor 1937997
 *
 */
public class PayrollManagement {
	/**
	 * is given a list of employees adn calculates the total expenses
	 * associated with their salaries
	 * @param employee arr with all the employees
	 * @return the total expenses
	 * @author Grigor 1937997
	 *
	 */
	public static double getTotalExpenses(Employee[] arr) {
		double total=0.0;
		for (Employee e : arr) {
			System.out.println( e.getWeeklyPay() );
			total += e.getWeeklyPay();
		}
		return total;
	}
	/**
	 * main method, calls the getTotalExpenses with an arr of employees
	 * and prints the result
	 * @author Grigor 1937997
	 *
	 */
	public static void main(String[] args) {
		Employee[] arr = new Employee[5];
		arr[0]=new SalariedEmployee(104000.0);
		arr[1]=new HourlyEmployee(20.5 , 13.5);
		arr[2]=new UnionizedHourlyEmployee(30.0 , 16.0 , 40.0 , 24.0 );
		arr[3]=new HourlyEmployee(45 , 15);
		arr[4]=new UnionizedHourlyEmployee(45, 15.0 , 40.0 , 22.5 );
		System.out.println(getTotalExpenses(arr) );
	}
}
