package workRelated;
/**
 * Object for  Unionized Hourly Employees
 * @author Grigor 1937997
 *
 */
public class UnionizedHourlyEmployee implements Employee {
	private double hoursWorked;
	private double hourlyPay;
	private double maxHoursPerWeek;
	private double overtimeRate;
	public UnionizedHourlyEmployee(double hoursWorked, double hourlyPay,double maxHoursPerWeek,double overtimeRate) {
		this.hourlyPay = hourlyPay;
		this.hoursWorked= hoursWorked;
		this.maxHoursPerWeek= maxHoursPerWeek;
		this.overtimeRate= overtimeRate;
	}
	/**
	 * get for the hours worked weekly 
	 * @return the hourse worked
	 * @author Grigor
	 */
	public double getHoursWorked() {
		return this.hourlyPay;
	}
	/**
	 * get for the pay per hour
	 * @return the hourly pay
	 * @author Grigor
	 */
	public double getHourlyPay() {
		return this.hoursWorked;
	}
	/**
	 * get for the maximum hours per week
	 * @return the max ours per week
	 * @author Grigor
	 */
	public double getMaxHoursPerWeek() {
		return this.maxHoursPerWeek;
	}
	/**
	 * get for the overtime pay per hour
	 * @return the overtime rate
	 * @author Grigor
	 */
	public double getOvertimerate() {
		return this.overtimeRate;
	}
	@Override
	public double getWeeklyPay() {
		if( this.hoursWorked <= this.maxHoursPerWeek) {
			return this.hoursWorked * this.hourlyPay;
		}
		double overtimeHours = this.hoursWorked -this.maxHoursPerWeek;
		double totalPay = this.maxHoursPerWeek * this.hourlyPay  +  overtimeHours * this.overtimeRate;
		return totalPay;
	}

}
