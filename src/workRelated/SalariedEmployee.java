package workRelated;
/**
 * Object for Yearly Employees
 * @author Grigor 1937997
 *
 */
public class SalariedEmployee implements Employee {
	private double yearlySalary;
	public SalariedEmployee(double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}
	/**
	 * get for the yearly salary 
	 * @return the yearly salary
	 * @author Grigor
	 */
	public double getYearlySalary() {
		return this.yearlySalary;
	}
	/**
	 * get for the weekly salary 
	 * @return the weekly salary
	 * @author Grigor
	 */
	@Override
	public double getWeeklyPay() {
		return this.yearlySalary / 52;
	}

}
