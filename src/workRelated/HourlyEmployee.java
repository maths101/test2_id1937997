package workRelated;
/**
 * Object for Hourly Employees
 * @author Grigor 1937997
 *
 */
public class HourlyEmployee implements Employee {
	private double hoursWorked;
	private double hourlyPay;
	public HourlyEmployee(double hoursWorked, double hourlyPay) {
		this.hourlyPay = hourlyPay;
		this.hoursWorked= hoursWorked;
	}
	/**
	 * get for the hours worked weekly 
	 * @return the hourse worked
	 * @author Grigor
	 */
	public double getHoursWorked() {
		return this.hourlyPay;
	}
	/**
	 * get for the pay per hour
	 * @return the hourly pay
	 * @author Grigor
	 */
	public double getHourlyPay() {
		return this.hoursWorked;
	}
	/**
	 * get for the weekly salary 
	 * @return the weekly salary
	 * @author Grigor
	 */
	@Override
	public double getWeeklyPay() {
		return this.hourlyPay * this.hoursWorked;
	}

}
