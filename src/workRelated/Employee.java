package workRelated;
/**
 * interface for employees
 * @author Grigor 1937997
 */
public interface Employee {
	/**
	 * method that all is-a Employee objects have to override so they can all return
	 * the weekly pay
	 * @return the weekly salary
	 * @author Grigor
	 */
	double getWeeklyPay() ;
}
