package utilities;

import java.util.*;
import space.Planet;
/**
 * Class with methods for Collections
 * @author Grigor 1937979
 *
 */
public class CollectionMethod {
	/**
	 * This method takes a collection of Planet object and 
	 * modifies it so that it only contains Planets that have a radius
	 * bigger than the size param
	 * @param a Collection of Planet to modify
	 * @param the minnimum size for the planets in the modified collection
	 * @return the modified Collection of Planets
	 * @author Grigor 1937979
	 *
	 */
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		
		HashSet<Planet> biggerPlanets = new HashSet<Planet>();
		for(Planet p : planets) {
			if(p.getRadius() >= size) {
				biggerPlanets.add(p);
			}
		}
		return biggerPlanets;
	}
}
